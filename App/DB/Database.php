<?php


namespace App\DB;

use \PDO;
use \PDOException;

class Database{


	const HOST = 'localhost';

	//nome do bd
	const NAME = 'clientedb';

	const USER = 'root';

	const PASS = '';

	// nome da tabela a ser manipulada
	private $table;

	private $connection;

	// define a tabela que irá trabalhar
	public function __construct($table = null){
		$this->table = $table;
		$this->setConnection();
	}

	private function setConnection(){
		try{
			$this->connection = new PDO('mysql:host='.self::HOST.';dbname='.self::NAME,self::USER,self::PASS);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

		}
		catch (PDOException $e){
			die('ERROR: '.$e->getMessage());
		}

	}

	// tenta executar as querys dentro do bd
	public function execute($query, $params = []){
		try{
			$statement = $this->connection->prepare($query);
			$statement->execute($params);
			return $statement;

		}
		catch (PDOException $e){
			die('ERROR: '.$e->getMessage());

		}
	}


	// insere valores no banco
	public function insert($values){

		// dados da query
		$fields = array_keys($values);

		//valores inseridos na tabela estao de acordo com os fields
		$binds = array_pad([], count($fields),'?');

		$query = 'INSERT INTO '.$this->table.'('.implode(',', $fields).') VALUES ('.implode(',', $binds).')';

		// executa o insert
		$this->execute($query, array_values($values));

		return $this->connection->lastInsertID();
	
	}

	// executa uma consulta no bd
	public function select($where = null, $order = null, $limit = null, $fields = '*'){

		$where = strlen($where) ? 'WHERE '.$where : '';
		$order = strlen($order) ? 'ORDER BY '.$order : '';
		$limit = strlen($limit) ? 'LIMIT '.$limit : '';



		$query = 'SELECT '.$fields.' FROM '.$this->table.' '.$where.' '.$order.' '.$limit;

		return $this->execute($query);

	}

	 public function update($where,$values){
	    //dados da query
	    $fields = array_keys($values);

	    //monta a query
	    $query = 'UPDATE '.$this->table.' SET '.implode('=?,',$fields).'=? WHERE '.$where;

	    $this->execute($query,array_values($values));

	    return true;
	  }

    public function delete($where){
    	
	    $query = 'DELETE FROM '.$this->table.' WHERE '.$where;

	    $this->execute($query);

	    return true;
	  }


}




 



