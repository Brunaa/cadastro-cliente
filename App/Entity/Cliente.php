<?php

namespace App\Entity;

use App\DB\Database;
use \PDO;

class Cliente{

	public $id;

	public $nome;

	public $idade;

	public $sexo;

	public $cpf;

	// salva clientes no bd
	public function salvarCliente(){

		//inserir cliente no banco
		$objetoDatabase = new Database('cliente');
		$this->id = $objetoDatabase-> insert([
										'nome' => $this->nome,
										'idade' => $this->idade,
										'sexo' => $this->sexo,
										'cpf' => $this->cpf
										]);

		return true;
	}

	public function atualizar(){
   		 return (new Database('cliente'))->update('id = '.$this->id,[
   		 								'nome' => $this->nome,
										'idade' => $this->idade,
										'sexo' => $this->sexo,
										'cpf' => $this->cpf
                                         ]);
    }

  	public function excluir(){
    	return (new Database('cliente'))->delete('id = '.$this->id);
    }

	// recebe os clientes do bd
	public static function getClientes($where = null, $order = 'nome', $limit = null){

		//seleciona todos os clientes
		return(new Database('cliente'))->select($where, $order, $limit)->fetchAll(PDO::FETCH_CLASS,self::class);

	}

	// busca cliente no bd pelo id
	public static function getCliente($id){
		
		return(new Database('cliente'))->select('id = '.$id)
									 ->fetchObject(self::class);


	}
	
}