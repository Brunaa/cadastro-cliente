<?php 

require __DIR__.'/vendor/autoload.php';

use App\Entity\Cliente;

$objCliente = Cliente::getCliente($_GET['id']);

if(isset($_POST['excluir']))
{

  $objCliente->excluir();

	header('location: index.php?status=sucess');
	exit;

}

include __DIR__.'/includes/header.php';
include __DIR__.'/includes/footer.php';
include __DIR__.'/includes/confirmarExclusao.php';
