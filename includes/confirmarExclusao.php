<main>

<div class="container">
  <h2 class="mt-3">Excluir Cliente</h2>

    <form method="post">

        <div class="form-group mb-3 ">

            <p>Você deseja realmente excluir o cliente: <strong><?=$objCliente->nome?></strong>?</p>

        </div>


        <div class="form-group">

            <button type="submit" name="excluir" class="btn btn-warning">Sim</button>

            <a href="index.php">
              <button type="button" class="btn btn-secondary">Não</button>
            </a>

        </div>

    </form>

  </div>

</main>