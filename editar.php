<?php 

require __DIR__.'/vendor/autoload.php';

define ('TITTLE', 'Editar Cliente');

use App\Entity\Cliente;

$objCliente = Cliente::getCliente($_GET['id']);

// Validação do POST
if(isset($_POST['nome'],$_POST['idade'],$_POST['sexo'],$_POST['cpf'])){
	
	$objCliente -> nome = $_POST['nome'];
	$objCliente -> idade = $_POST['idade'];
	$objCliente -> sexo = $_POST['sexo'];
	$objCliente -> cpf = $_POST['cpf'];
	$objCliente -> atualizar();


	header('location: index.php?status=sucess');
	exit;

	

}

include __DIR__.'/includes/header.php';
include __DIR__.'/includes/footer.php';
include __DIR__.'/includes/formCliente.php';
